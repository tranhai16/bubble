import { Color, Graphics, Vec2, Vec3 } from "cc";
import GameUtils from "../utils/GameUtils";

export default class Circle {
    readonly position: Vec2 = new Vec2();
    radius: number = 0;
    fill: boolean = false;
    stroke: number = 4;
    color: Color = Color.CYAN;

    constructor(position: Vec2, radius: number) {
        this.position.set(position);
        this.radius = radius;
    }

    contains(point: Vec2): boolean {
        return Vec2.distance(point, this.position) < this.radius;
    }

    draw(graphics: Graphics) {
        if (this.radius === 0) return;
        graphics.lineWidth = this.stroke;
        graphics.circle(this.position.x, this.position.y, this.radius)
        graphics.strokeColor = this.color;
        graphics.stroke();
        if (this.fill) {
            graphics.fillColor.set(this.color);
            graphics.fill();
        }
    }
}