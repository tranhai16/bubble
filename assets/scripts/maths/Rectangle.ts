import { Size } from "cc";
import LineSegment from "./LineSegment";

export default class Rectangle {
    set(params: { left?: number; right?: number; top?: number; bottom?: number; }
        = {
            left: this.left,
            right: this.right,
            bottom: this.bottom,
            top: this.top
        }) {
        this.left = params.left;
        this.right = params.right;
        this.top = params.top;
        this.bottom = params.bottom;
    }
    public left: number = 0;
    public right: number = 0;
    public top: number = 0;
    public bottom: number = 0;
    public get width(): number {
        return this.right - this.left;
    }
    public get height(): number {
        return this.top - this.bottom;
    }

    constructor(params: { 
        left?: number;
        right?: number;
        top?: number;
        bottom?: number 
    } = { left: 0, right: 0, top: 0, bottom: 0 }) {
        this.left = params.left;
        this.right = params.right;
        this.top = params.top;
        this.bottom = params.bottom;
    }

    getLineSegments() : LineSegment[] {

        return [
            new LineSegment().setCoordinates(this.left, this.top, this.right, this.top),
            new LineSegment().setCoordinates(this.left, this.bottom, this.right, this.bottom),
            new LineSegment().setCoordinates(this.left, this.top, this.left, this.bottom),
            new LineSegment().setCoordinates(this.right, this.top, this.right, this.bottom)
        ]
    }

    getSize() : Size {
        return new Size(this.right - this.left, this.top - this.bottom);
    }
}