import { Vec2 } from "cc";

export default class Line {
    public readonly point1: Vec2;
    public readonly point2: Vec2;

    constructor(point1: Vec2 = new Vec2(), point2: Vec2 = new Vec2()) {
        this.point1 = point1;
        this.point2 = point2;
    }

    set(p1: Vec2, p2: Vec2): void {
        this.point1.set(p1.x, p1.y);
        this.point2.set(p2.x, p2.y);
    }
}