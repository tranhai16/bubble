import { Vec2 } from "cc";
import LineSegment from "./LineSegment";

export default class Geometry {
    static readonly EPSILON: number = 0.000001;

    static crossProduct(a: Vec2, b: Vec2): number {
        return a.x * b.y - b.x * a.y;
    }

    static doBoundingBoxesIntersect(a: Vec2[], b: Vec2[]): boolean {
        return a[0].x <= b[1].x && a[1].x >= b[0].x && a[0].y <= b[1].y && a[1].y >= b[0].y;
    }

    static isPointOnLine(a: LineSegment, b: Vec2): boolean {
        const aTmp = new LineSegment(new Vec2(0, 0), new Vec2(a.end.x - a.start.x, a.end.y - a.start.y));
        const bTmp = new Vec2(b.x - a.start.x, b.y - a.start.y);
        const r = Geometry.crossProduct(aTmp.end, bTmp);
        return Math.abs(r) < Geometry.EPSILON;
    }

    static isPointRightOfLine(a: LineSegment, b: Vec2): boolean {
        const aTmp = new LineSegment(new Vec2(0, 0), new Vec2(a.end.x - a.start.x, a.end.y - a.start.y));
        const bTmp = new Vec2(b.x - a.start.x, b.y - a.start.y);
        return Geometry.crossProduct(aTmp.end, bTmp) < 0;
    }

    static lineSegmentTouchesOrCrossesLine(a: LineSegment, b: LineSegment): boolean {
        return Geometry.isPointOnLine(a, b.start) || Geometry.isPointOnLine(a, b.end) ||
            (Geometry.isPointRightOfLine(a, b.start) !== Geometry.isPointRightOfLine(a, b.end));
    }

    static doLinesIntersect(a: LineSegment, b: LineSegment): boolean {
        const box1 = a.getBoundingBox();
        const box2 = b.getBoundingBox();
        return Geometry.doBoundingBoxesIntersect(box1, box2) &&
            Geometry.lineSegmentTouchesOrCrossesLine(a, b) &&
            Geometry.lineSegmentTouchesOrCrossesLine(b, a);
    }

    static findIntersection(l1: LineSegment, l2: LineSegment): Vec2 {
        const a1 = l1.end.y - l1.start.y;
        const b1 = l1.start.x - l1.end.x;
        const c1 = a1 * l1.start.x + b1 * l1.start.y;

        const a2 = l2.end.y - l2.start.y;
        const b2 = l2.start.x - l2.end.x;
        const c2 = a2 * l2.start.x + b2 * l2.start.y;

        const delta = a1 * b2 - a2 * b1;
        return new Vec2((b2 * c1 - b1 * c2) / delta, (a1 * c2 - a2 * c1) / delta);
    }

    static onSegment(p: Vec2, q: Vec2, r: Vec2): boolean {
        return q.x <= Math.max(p.x, r.x) && q.x >= Math.min(p.x, r.x) &&
            q.y <= Math.max(p.y, r.y) && q.y >= Math.min(p.y, r.y);
    }
}
