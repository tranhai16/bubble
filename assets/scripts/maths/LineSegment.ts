import { Vec2 } from "cc";
import Geometry from "./Geometry";
import Line from "./Line";

export default class LineSegment {
    public start: Vec2;
    public end: Vec2;
    public name: string;

    constructor(a: Vec2 = new Vec2(), b: Vec2 = new Vec2(), name: string = "LineSegment") {
        this.start = a;
        this.end = b;
        this.name = name;
    }


    getBoundingBox(): Vec2[] {
        const result: Vec2[] = [];
        result[0] = new Vec2(Math.min(this.start.x, this.end.x), Math.min(this.start.y, this.end.y));
        result[1] = new Vec2(Math.max(this.start.x, this.end.x), Math.max(this.start.y, this.end.y));
        return result;
    }

    toString(): string {
        if (this.name === "LineSegment") {
            return `LineSegment [${this.start.x},${this.start.y}] to [${this.end.x},${this.end.y}]`;
        } else {
            return this.name;
        }
    }

    // hashCode(): number {
    //     const prime = 31;
    //     let result = 1;
    //     result = prime * result + (this.start === null ? 0 : this.start.hashCode());
    //     result = prime * result + (this.name === null ? 0 : this.name.hashCode());
    //     result = prime * result + (this.end === null ? 0 : this.end.hashCode());
    //     return result;
    // }

    equals(obj: any): boolean {
        if (this === obj) return true;
        if (obj === null || this.constructor !== obj.constructor) return false;
        const other = obj as LineSegment;
        if (this.start === null) {
            if (other.start !== null) return false;
        } else if (!this.start.equals(other.start)) return false;
        if (this.name === null) {
            if (other.name !== null) return false;
        } else if (this.name !== other.name) return false;
        if (this.end === null) {
            return other.end === null;
        } else return this.end.equals(other.end);
    }

    toLine(): Line {
        return new Line(this.start, this.end);
    }

    setCoordinates(f1: number, f2: number, s1: number, s2: number): LineSegment {
        this.start.set(f1, f2);
        this.end.set(s1, s2);
        return this;
    }

    containPoint(point: Vec2): boolean {
        return Geometry.onSegment(this.start, point, this.end) || Geometry.onSegment(this.end, point, this.start);
    }

    isContainNode(other: LineSegment): boolean {
        if (
            Geometry.doLinesIntersect(this, other) &&
            Geometry.lineSegmentTouchesOrCrossesLine(this, other) &&
            Geometry.lineSegmentTouchesOrCrossesLine(other, this)
        ) {
            return this.containPoint(other.start) || this.containPoint(other.end);
        }
        return false;
    }
}
