import { _decorator, Color, Component, Enum, ERaycast2DType, EventTouch, game, geometry, Graphics, Input, input, instantiate, Label, Node, ParticleSystem2D, physics, PhysicsSystem2D, Prefab, Size, tween, UIOpacity, UITransform, v2, v3, Vec2, Vec3 } from 'cc';
import Const, { ColorDefines, NearestFindObject } from './models/Const';
import Level from './models/Level';
import { MapComponent } from './models/MapComponent';
import { BubbleComponent } from './models/BubbleComponent';
import Rectangle from './maths/Rectangle';
import LineSegment from './maths/LineSegment';
import GameUtils from './utils/GameUtils';
import Geometry from './maths/Geometry';
import ColorPrefab from './models/ColorPrefab';
const { ccclass, property } = _decorator;

@ccclass('GameManager')
export class GameManager extends Component {
    @property(MapComponent)
    map: MapComponent;
    @property(Node)
    pinNode: Node;
    @property(Graphics)
    graphics: Graphics;

    @property(Prefab)
    boomParticlePrefab: Prefab;

    @property(Prefab)
    scorePrefab: Prefab;
    private static currentColor: ColorDefines;
    private static currentLevel: number = 80;
    private levels: Level[] = [];
    private _realdy: boolean = false;
    private _startPin: boolean = false;
    private static previewBubble: Node;
    private readonly shootLine: {
        origin: Vec2,
        paths: Vec2[],
    } = { origin: new Vec2(0, 0), paths: [] };
    private readonly startPosition: Vec2 = new Vec2(0, 0);
    private readonly size: Size = new Size();
    private readonly bound: Rectangle = new Rectangle();
    private readonly shootSpeed: number = 100;

    protected onLoad(): void {
        Const.initColorPrefabs(() => {
            GameUtils.getLevels('./level', 'level', (data: Level[]) => {
                this.levels = data;
                this.startPosition.set(GameUtils.v3ToV2(this.pinNode.getWorldPosition()));
                this.shootLine.origin.set(this.startPosition);
                this.size.set(this.node.getComponent(UITransform).contentSize);
                this.initView();
            });
        });

        input.on(Input.EventType.TOUCH_START, this.onTouchStart, this);
        input.on(Input.EventType.TOUCH_MOVE, this.onTouchMove, this);
        input.on(Input.EventType.TOUCH_END, this.onTouchEnd, this);
        input.on(Input.EventType.TOUCH_CANCEL, this.onTouchEnd, this);
    }

    protected onDestroy(): void {
        input.off(Input.EventType.TOUCH_START, this.onTouchStart, this);
        input.off(Input.EventType.TOUCH_MOVE, this.onTouchMove, this);
        input.off(Input.EventType.TOUCH_END, this.onTouchEnd, this);
        input.off(Input.EventType.TOUCH_CANCEL, this.onTouchEnd, this);
    }

    update(deltaTime: number) {
        if (this.shootLine.paths.length > 0) {
            let uiTransform = this.node.getComponent(UITransform);
            let size = uiTransform.contentSize;
            let anchor = uiTransform.anchorPoint;
            this.graphics.clear();
            this.graphics.lineWidth = 6;
            this.graphics.moveTo(this.shootLine.origin.x - size.width * anchor.x, this.shootLine.origin.y - size.height * anchor.y);
            this.shootLine.paths.forEach(path => {
                this.graphics.lineTo(path.x - size.width * anchor.x, path.y - size.height * anchor.y);
            })
            this.graphics.strokeColor = Color.GREEN;
            this.graphics.stroke();
        }
    }

    public onTouchStart(event: EventTouch) {
        if (!this._realdy) return;
        if (event.getTouches().length === 1) {
            this.initStartPin(event.getUILocation());
        }
    }

    public onTouchMove(event: EventTouch) {
        if (!this._realdy) return;
        if (this._startPin && event.getTouches().length === 1) {
            this.calculatePin(event.getUILocation());
        }
    }

    public onTouchEnd(event: EventTouch) {
        if (!this._realdy) return;
        if (this._startPin && event.getTouches().length === 1) {
            this.endPin(event);
        }
    }

    private initView() {
        let level = this.levels.find(lev => lev.level === GameManager.currentLevel);
        if (!level) return;
        this.map.setLevel(level, () => {
            this.generatePin();
        });
    }

    private initStartPin(arg0: Vec2) {
        if (this._startPin) return;
        let radius = this.pinNode.children[0].getComponent(UITransform).width / 2;
        if (Vec2.distance(arg0, GameUtils.v3ToV2(this.pinNode.getWorldPosition())) <= radius) {
            this._startPin = true;
        }

    }

    private calculatePin(arg0: Vec2) {
        if (GameManager.previewBubble != null) GameManager.previewBubble.removeFromParent();
        this.shootLine.paths = [];
        const radius = this.map.radiusTile;;
        const mapWorldBound = this.map.getWorldBound();
        this.bound.set({ left: radius, right: this.size.width - radius, top: mapWorldBound.top, bottom: 0 });
        const dir = arg0.clone().subtract(this.startPosition).normalize();
        const maxDistance = v2(this.bound.width, this.bound.height).lengthSqr();
        const others = this.map.allChildrenWorldPosition();
        const result = GameUtils.findGoal(
            this.bound,
            this.startPosition,
            dir,
            maxDistance,
            GameUtils.v3ToV2s(others),
            radius,
            (intersectPoint: Vec2) => {
                this.shootLine.paths.push(intersectPoint);
            }
        );
        if (result.length === 0) return;
        const closest: Vec2 = result[0];
        const origin: Vec2 = result[1];
        dir.set(result[2]);
        const nearest = GameUtils.nearest(origin, GameUtils.rayCastCirclePoints(origin, dir, maxDistance, closest, radius));
        if (!nearest) return;
        this.shootLine.paths.push(nearest);
        const nodeClosest = this.map.allChildrenWorldPosition().find(pos => GameUtils.equalsV3(pos, GameUtils.v2ToV3(closest)));
        if (!nodeClosest) return;
        const previewPosition = GameUtils.getOtherPositionV2(nearest, closest, radius, GameUtils.v3ToV2s(others));
        if (!previewPosition) return;
        GameManager.previewBubble = instantiate(Const.colorPrefabMap.get(GameManager.currentColor));
        this.node.addChild(GameManager.previewBubble);
        GameManager.previewBubble.getComponent(UITransform).contentSize.set(radius * 2, radius * 2);
        GameManager.previewBubble.setWorldPosition(GameUtils.v2ToV3(previewPosition));
        GameManager.previewBubble.getComponent(UIOpacity).opacity /= 2;
    }

    private endPin(event: EventTouch) {
        this._startPin = false;
        this.shoot();
        if (GameManager.previewBubble != null) {
            GameManager.previewBubble.removeFromParent();
        }
        this.shootLine.paths = [];
        this.graphics.clear();
    }

    private shoot() {
        const pin = this.pinNode.children[0];
        const tw = tween(pin);
        const pos = GameManager.previewBubble.getWorldPosition();
        const allPositions = this.map.allChildrenWorldPosition();
        const self = this;
        const previousPosition = pin.getWorldPosition();
        let isDone = false;
        this.shootLine.paths.forEach((path, index) => {
            if (index > 0) {
                previousPosition.set(v3(this.shootLine.paths[index - 1].x, this.shootLine.paths[index - 1].y, 0));
            }
            let nextPosition = v3(path.x, path.y, 0)
            let time = Vec3.distance(nextPosition, previousPosition) * game.deltaTime / this.shootSpeed;
            if (time < 0.1) time = 0.1;
            tw.to(time, { worldPosition: nextPosition }, {
                onUpdate(target, ratio) {
                    let currentPosition = pin.getWorldPosition();
                    let collider = GameUtils.checkCollider(currentPosition, allPositions, self.map.radiusTile);
                    if (collider) {
                        let previewPosition = GameUtils.getOtherPositionV3(currentPosition, collider, self.map.radiusTile, allPositions);
                        if (previewPosition && !isDone) {
                            isDone = true;
                            setTimeout(() => {
                                pos.set(previewPosition);
                                pin.setWorldPosition(pos);
                                self.addAndRemove(pos);
                            }, 0.05);
                            tw.stop();
                            tw.removeSelf();
                        }
                    }
                },
            });
        });
        tw.call(() => {
            if (!isDone) {
                isDone = true;
                setTimeout(() => {
                    pin && pin.setWorldPosition(pos), this.addAndRemove(pos);
                }, 0.05);
                
            }
        }).start();
    }

    private addAndRemove(worldPosition: Vec3) {
        this._realdy = false;
        const newNode = instantiate(Const.colorPrefabMap.get(GameManager.currentColor));
        this.map.node.addChild(newNode);
        newNode.getComponent(UITransform).contentSize.set(this.map.radiusTile * 2, this.map.radiusTile * 2);
        newNode.setWorldPosition(worldPosition);
        let heighestWorldPositions = this.map.heighestWorldPositions();
        const data = this.map.node.getComponentsInChildren(BubbleComponent).map(cp => {
            return {
                position: cp.node.getWorldPosition(),
                color: cp.colorType
            }
        });
        const outDeletes: NearestFindObject[] = GameUtils.findNearestAndSameColors(
            { position: worldPosition, color: GameManager.currentColor },
            this.map.radiusTile,
            data
        );
        let positionArr: Vec3[] = [];
        if (outDeletes.length > 2) outDeletes.forEach(outDelete => {
            const p = outDelete.position.clone();
            positionArr.push(p);
            let heighestWorldPosition = heighestWorldPositions.find(position => GameUtils.equalsV3(position, p));
            if (heighestWorldPosition) {
                heighestWorldPositions.splice(heighestWorldPositions.indexOf(heighestWorldPosition), 1);
            }
            this.addBoomEffect(p);
            this.map.removeChildByWorldPosition(p);
        });
        this.pinNode.removeAllChildren();
        positionArr.forEach((p) => {
            this.addScore(1, p);
        });
        this.map.updateOrphanBubbles(
            heighestWorldPositions,
            this.pinNode.getWorldPosition().y,
            (deletePositions?: Vec3[]) => {
                deletePositions && deletePositions.forEach(deletePos => {
                    this.addBoomEffect(deletePos);
                    this.addScore(1, deletePos);
                })
                this.map.updateView(() => {
                    this.generatePin(false);
                });
            });

    }

    private generatePin(removeAllChildren: boolean = true) {
        removeAllChildren && this.pinNode.removeAllChildren();
        let levelColorPrefabs = Array.from(this.map.levelColorPrefabs());
        if (levelColorPrefabs.length == 0) return;
        let index = Math.floor(Math.random() * levelColorPrefabs.length);
        GameManager.currentColor = levelColorPrefabs[index];
        let pinBubble = instantiate(Const.colorPrefabMap.get(GameManager.currentColor));
        this.pinNode.addChild(pinBubble);
        pinBubble.getComponent(UITransform).contentSize.set(this.map.radiusTile * 2, this.map.radiusTile * 2);
        this.startPosition.set(GameUtils.v3ToV2(this.pinNode.getWorldPosition()));
        this.shootLine.origin.set(this.startPosition);
        this._realdy = true;
    }

    private addBoomEffect(position: Vec3) {
        const boomParticle = instantiate(this.boomParticlePrefab);
        this.node.addChild(boomParticle);
        boomParticle.setWorldPosition(position);
    }
    private addScore(score: number, worldPosition: Vec3): void {
        const scoreNode = instantiate(this.scorePrefab);
        this.node.addChild(scoreNode);
        scoreNode.getComponent(Label).string = `+${score}`;
        scoreNode.setWorldPosition(worldPosition);
        tween(scoreNode).to(0.5, { worldPosition: worldPosition.clone().add3f(0, 50, 0) })
            .call(() => scoreNode.removeFromParent()).start();
    }
}


