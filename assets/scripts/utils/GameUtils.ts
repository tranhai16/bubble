import { TextAsset, Vec2, Vec3, resources, v2, v3 } from "cc";
import Level from "../models/Level";
import Const, { NearestFindObject } from "../models/Const";
import Geometry from "../maths/Geometry";
import LineSegment from "../maths/LineSegment";
import Rectangle from "../maths/Rectangle";

export default class GameUtils {


    static readonly primePoints: Vec2[] = [
        v2(1, 0),
        v2(-1, 0),
        v2(Const.sin30, Const.cos30),
        v2(-Const.sin30, Const.cos30),
        v2(-Const.sin30, -Const.cos30),
        v2(Const.sin30, -Const.cos30)
    ];

    static rayCastCirclePoints(
        origin: Vec2,
        dir: Vec2,
        maxDistance: number,
        center: Vec2,
        radius: number
    ): Vec2[] {
        const end = origin.clone().add(dir.clone().multiplyScalar(maxDistance));
        const intersectionPoints: Vec2[] = [];

        const baX = end.x - origin.x;
        const baY = end.y - origin.y;
        const caX = center.x - origin.x;
        const caY = center.y - origin.y;

        const a = baX * baX + baY * baY;
        const bBy2 = baX * caX + baY * caY;
        const c = caX * caX + caY * caY - radius * radius;

        const pBy2 = bBy2 / a;
        const q = c / a;

        const disc = pBy2 * pBy2 - q;
        if (disc < 0) {
            return intersectionPoints;
        }

        const tmpSqrt = Math.sqrt(disc);
        const abScalingFactor1 = -pBy2 + tmpSqrt;
        const abScalingFactor2 = -pBy2 - tmpSqrt;

        const p1 = new Vec2(origin.x - baX * abScalingFactor1, origin.y - baY * abScalingFactor1);
        if (disc === 0) {
            intersectionPoints.push(p1);
            return intersectionPoints;
        }

        const p2 = new Vec2(origin.x - baX * abScalingFactor2, origin.y - baY * abScalingFactor2);
        intersectionPoints.push(p1, p2);
        return intersectionPoints;
    }

    static nearest(origin: Vec2, others: Vec2[] | null): Vec2 | null {
        if (!others || others.length === 0) return null;

        const sortedList = others.slice().sort((a, b) => {
            const distanceA = Vec2.distance(a, origin);
            const distanceB = Vec2.distance(b, origin);
            return distanceA - distanceB;
        });

        return sortedList[0];
    }

    static rayCast(
        origin: Vec2,
        dir: Vec2,
        maxDistance: number,
        others: Vec2[],
        radius: number
    ): Vec2[] {
        return others.filter((vec2) => GameUtils.rayCastCirclePoints(origin, dir, maxDistance, vec2, radius).length > 0);
    }

    static rayCastClosest(
        origin: Vec2,
        dir: Vec2,
        maxDistance: number,
        others: Vec2[],
        radius: number
    ): Vec2 | null {
        const intersectionPoints = GameUtils.rayCast(origin, dir, maxDistance, others, radius);
        return GameUtils.nearest(origin, intersectionPoints);
    }

    static getLevels(levelFileParentDir: string, fileNameWithoutExtention: string, onDone: (data: Level[]) => void) {
        const levels: Level[] = [];
        resources.loadDir(levelFileParentDir, (err, data: TextAsset[]) => {
            const result = data.find(x => x.name === fileNameWithoutExtention);
            const levelTexts = result.text.trim().split("\r\n\r\n");
            levelTexts.forEach(lvText => {
                let textRows = lvText.trim().split("\r\n");
                let lv = parseInt(textRows[0].substring(2, lvText.length));
                let rows = parseInt(textRows[1]);
                let level = new Level(lv, rows);
                level.grid.forEach((levelRow, index) => {
                    let colData = textRows[index + 2].trim().split(".");
                    colData.forEach(col => {
                        if (col.length > 0) {
                            levelRow.push(parseInt(col));
                        }
                    })
                })
                levels.push(level);
            })
            onDone && onDone(levels);
        })
    }
    static findGoal(bound: Rectangle, origin: Vec2, dir: Vec2, maxDistance: number, objects: Vec2[], radius: number,
        update?: (intersectPoint: Vec2) => void) : Vec2[] {
        const closest = GameUtils.rayCastClosest(origin, dir, maxDistance, objects, radius);
        if (closest != null) {
            return [closest, origin, dir];
        } else {
            const laser = new LineSegment(origin.clone(), origin.clone().add(dir.clone().multiplyScalar(maxDistance)));
            const intersectPoint = new Vec2(origin.x, origin.y);
            for (const line of bound.getLineSegments()) {
                if (!line.containPoint(origin.clone())
                    && Geometry.doLinesIntersect(laser, line)
                    && Geometry.lineSegmentTouchesOrCrossesLine(laser, line)
                    && Geometry.lineSegmentTouchesOrCrossesLine(line, laser)) {
                    intersectPoint.set(Geometry.findIntersection(laser, line));
                    if (intersectPoint.x < bound.left) intersectPoint.x = bound.left;
                    if (intersectPoint.x > bound.left + bound.width) intersectPoint.x = bound.left + bound.width;
                    dir.multiply2f(-1, 1);
                    update && update(intersectPoint)
                    if (line.containPoint(new Vec2(bound.left + bound.width / 2, bound.top))
                        || line.containPoint(new Vec2(bound.left + bound.width / 2, bound.top + bound.height))) {
                        return [];
                    }
                    return this.findGoal(bound, intersectPoint, dir, maxDistance, objects, radius, update);
                }
            }
            return [];
        }
    }
    static getOtherPositionV3(position: Vec3, origin: Vec3, radius: number, allPositions: Vec3[]): Vec3 {
        let vec3 = new Vec3();
        let minDistance = Infinity;
        let currentPrime = new Vec3();
        GameUtils.primePoints.forEach(point => {
            vec3.set(origin.clone().add(v3(point.x, point.y, 0).multiplyScalar(radius * 2)));
            let distance = Vec3.distance(position, vec3);
            let primePosition = allPositions.find(pos => GameUtils.equalsV3(pos, vec3));
            if (distance < minDistance && !primePosition) {
                minDistance = distance;
                currentPrime.set(vec3);
            }
        });
        if (!currentPrime.equals(Vec3.ZERO)) {
            return currentPrime;
        }
        return null;
    }
    static getOtherPositionV2(position: Vec2, origin: Vec2, radius: number, allPositions: Vec2[]): Vec2 {
        let v = GameUtils.getOtherPositionV3(
            GameUtils.v2ToV3(position),
            GameUtils.v2ToV3(origin),
            radius,
            allPositions.map(v => GameUtils.v2ToV3(v))
        );
        if (v) return GameUtils.v3ToV2(v);
        return null;
    }
    static v2ToV3(vec2: Vec2) : Vec3{
        return v3(vec2.x, vec2.y, 0);
    }
    static v2ToV3s(vec2: Vec2[]) : Vec3[] {
        return vec2.map(v => v3(v.x, v.y, 0));
    }
    static v3ToV2(vec3: Vec3) : Vec2{
        return v2(vec3.x, vec3.y); 
    }
    static v3ToV2s(vec3: Vec3[]) : Vec2[] {
        return vec3.map(v => v2(v.x, v.y));
    }

    static checkCollider(arg0: Vec3, allPositions: Vec3[], radius: number): Vec3 {
        return allPositions.find(v => Vec3.distance(arg0, v) <= radius * 2);
    }
    static findNearestAndSameColors(
        start: NearestFindObject,
        radius: number,
        all: NearestFindObject[]): NearestFindObject[] {
        const out: NearestFindObject[] = [];
        function find(origin: NearestFindObject) {
            all.filter(cp =>
                !GameUtils.equalsV3(origin.position, cp.position) && GameUtils.isNearestAndSameColor(origin, cp, radius)).forEach(cp => {
                    if (!out.find(cpn => cpn === cp)) {
                        out.push(cp);
                        find(cp);
                    }
                });
        }
        find(start);
        return out;
    }

    static isNearestAndSameColor(start: NearestFindObject, bb: NearestFindObject, radius: number): boolean {
        return GameUtils.distanceEqualsV3(start.position, bb.position, radius * 2) && bb.color === start.color;
    }

    static findNearest(start: Vec3, radius: number, allPositions: Vec3[]): Vec3[] {
        const out : Vec3[] = [];
        const find = (origin: Vec3) => {
            allPositions.filter(position =>
                GameUtils.distanceEqualsV3(position, origin, radius * 2))
                .forEach(position => {
                    if(!out.find(p => GameUtils.equalsV3(position, p))) {
                        out.push(position);
                        find(position);
                    }
                });
        }
        find(start);
        return out;
    }

    static equalsV3(v1: Vec3, v2: Vec3): boolean {
        return Vec3.distance(v1, v2) <= Const.DEVIATION;
    }
    static equalsV2(v1: Vec2, v2: Vec2): boolean {
        return Vec2.distance(v1, v2) <= Const.DEVIATION;
    }

    static distanceEqualsV3(v1: Vec3, v2: Vec3, distance: number): boolean {
        return Vec3.distance(v1, v2) <= distance + Const.DEVIATION
            && Vec3.distance(v1, v2) >= distance - Const.DEVIATION;
    }

    static distanceEqualsV2(v1: Vec2, v2: Vec2, distance: number): boolean {
        return Vec2.distance(v1, v2) <= distance + Const.DEVIATION
            && Vec2.distance(v1, v2) >= distance - Const.DEVIATION;
    }

    static calculateMinPosition(positions: Vec3[]): Vec3 {
        let mapX = positions.map(p => p.x);
        let mapY = positions.map(p => p.y);
        mapX.sort((a, b) => a - b);
        mapY.sort((a, b) => a - b);
        return v3(mapX[0], mapY[0], 0);
    }

    static calculateMaxPosition(positions: Vec3[]): Vec3 {
        let mapX = positions.map(p => p.x);
        let mapY = positions.map(p => p.y);
        mapX.sort((a, b) => b - a);
        mapY.sort((a, b) => b - a);
        return v3(mapX[0], mapY[0], 0);
    }
}