import { _decorator, Color, Component, EditBox, game, Graphics, instantiate, Node, Prefab, UITransform, v3, Vec3, Widget } from 'cc';
import Const from '../models/Const';
import Level from '../models/Level';
import GameUtils from '../utils/GameUtils';
import { BubbleComponent } from '../models/BubbleComponent';
import Rectangle from '../maths/Rectangle';
import Circle from '../maths/Circle';
const { ccclass, property } = _decorator;

@ccclass('GenManager')
export class GenManager extends Component {

    @property(EditBox)
    editBox: EditBox;
    @property(Node)
    content: Node;
    @property(Graphics)
    previewGraphics: Graphics;

    private _level: number = 1;
    private _levelData: Level[] = [];
    radiusTile: number = 0;
    listCircle: Circle[] = [];
    protected onLoad(): void {

        Const.initColorPrefabs(() => {
            GameUtils.getLevels('./level', 'level', (data: Level[]) => {
                this._levelData = data;
                this.updateLevel();
            });
        });
    }

    start() {

    }

    update(deltaTime: number) {

    }

    onEditReturn() {
        let chooseLevel = parseInt(this.editBox.string);
        if (chooseLevel > 100) chooseLevel = 100;
        else if (chooseLevel < 1) chooseLevel = 1;
        if (chooseLevel && chooseLevel > 0 && chooseLevel - 1 < this._levelData.length) {
            this._level = chooseLevel;
            this.updateLevel();
        }

    }

    private updateLevel() {
        this.editBox.string = this._level.toString();
        this.setLevel(this._levelData.find(lv => lv.level === this._level));
    }
    setLevel(level: Level) {
        let nodeUITransform = this.node.getComponent(UITransform);
        let nodeSize = nodeUITransform.contentSize;
        let anchor = nodeUITransform.anchorPoint;
        this.content.removeAllChildren();
        const uiTransform = this.content.getComponent(UITransform);
        const maxCols = level.getMaxCols();
        const radius = uiTransform.contentSize.width / (maxCols * 2);
        this.radiusTile = radius;
        const size = radius * 2;
        let maxWidth = maxCols * size;
        let startX = -maxWidth / 2 + radius;
        let grid = level.grid.reverse();
        let allPositions: Vec3[] = [];
        this.listCircle = [];
        this.content.getComponent(Widget).bottom = 0;
        this.content.getComponent(Widget).updateAlignment();
        for (let i = 0; i < level.rows; i++) {
            const cols = grid[i].length;
            grid[i].forEach((col, index) => {
                const position = v3(startX + size * index + (cols === maxCols ? 0 : size * 0.5),
                    i * size * Math.cos(Math.PI / 6) + radius, 0);
                let colorPrefab: Prefab = Const.colorPrefabMap.get(col);
                if (colorPrefab) {
                    const bubbleNode = instantiate(colorPrefab);
                    this.content.addChild(bubbleNode);
                    bubbleNode.getComponent(BubbleComponent).radius = radius;
                    allPositions.push(position);
                    bubbleNode.setPosition(position.clone());
                    this.listCircle.push(new Circle(
                        GameUtils.v3ToV2(bubbleNode.getWorldPosition().add3f(
                            -nodeSize.width * anchor.x,
                            -nodeSize.height * anchor.y,
                            0
                        )),
                        radius
                    ));
                }
                else {
                    const node = new Node();
                    this.content.addChild(node);
                    node.setPosition(position);
                    this.listCircle.push(new Circle(GameUtils.v3ToV2(node.getWorldPosition().add3f(
                        -nodeSize.width * anchor.x,
                        -nodeSize.height * anchor.y,
                        0
                    )), radius));
                    node.removeFromParent();
                }

            });
        }
        let bound = this.getBound(this.content, allPositions);
        this.content.getComponent(UITransform).contentSize.set(bound.width, bound.height);
        this.updateGraphicsDraw();
    }
    updateGraphicsDraw() {

        this.previewGraphics.clear();
        this.previewGraphics.lineWidth = 4;
        this.listCircle.forEach(circle => {
            circle.draw(this.previewGraphics)
        })
        this.previewGraphics.strokeColor = Color.CYAN;
        this.previewGraphics.stroke();
    }

    onNextClick() {
        this._level++;
        if (this._level > this._levelData.length) {
            this._level = this._levelData.length;
            return;
        }
        this.updateLevel();
    }
    onPrevClick() {
        this._level--;
        if (this._level < 1) {
            this._level = 1;
            return;
        }
        this.updateLevel();
    }
    onExportClick() { }
    public getBound(root: Node, positions?: Vec3[]): Rectangle {
        !positions && (positions = root.children.map(data => data.getPosition()));
        const maxPosition = GameUtils.calculateMaxPosition(positions);
        const minPosition = GameUtils.calculateMinPosition(positions);
        return new Rectangle({
            left: minPosition.x - this.radiusTile,
            right: maxPosition.x + this.radiusTile,
            top: maxPosition.y + this.radiusTile,
            bottom: minPosition.y - this.radiusTile
        });
    }

    getRectBound(root: Node): Rectangle {
        const uiTransform = root.getComponent(UITransform);
        return new Rectangle({
            left: this.content.worldPosition.x
                - uiTransform.anchorX
                * uiTransform.contentSize.width,
            right: this.content.worldPosition.x
                - uiTransform.anchorX
                * uiTransform.contentSize.width
                + uiTransform.contentSize.width,
            top: this.content.worldPosition.y
                - uiTransform.anchorY
                * uiTransform.contentSize.height
                + uiTransform.contentSize.height,
            bottom: this.content.worldPosition.y
                - uiTransform.anchorY
                * uiTransform.contentSize.height
        });
    }
}


