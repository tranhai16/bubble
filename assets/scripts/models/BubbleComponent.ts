import { _decorator, Component, Node, SpriteFrame, Sprite, Vec2, v2, UITransform, Vec3, v3, Enum } from 'cc';
import { ColorDefines } from './Const';
import GameUtils from '../utils/GameUtils';
const { ccclass, property } = _decorator;

@ccclass('BubbleComponent')
export class BubbleComponent extends Component {

    @property({type:Enum(ColorDefines)})
    public colorType: ColorDefines;
    private _radius: number = 0;

    public set radius(value: number) {
        this._radius = value;
        this.node.getComponent(UITransform).contentSize.set(value * 2, value * 2);
    }

    protected onLoad(): void {
    }

    start() {

    }

    update(deltaTime: number) {
        
    }


    
}


