import { Color, Prefab, Vec3, resources } from "cc";

export enum ColorDefines {
    RED = 1,
    SKY = 2,
    YELLOW = 3,
    GREEN = 4,
    ORANGE = 5,
    PINK = 6,
    TRANSPARENT = 7,
}

export default class Const {
    static readonly sin30 = Math.sin(Math.PI / 6);
    static readonly cos30 = Math.cos(Math.PI / 6);
    static readonly DEVIATION = 0.000001; 

    static readonly colorPrefabMap : Map<ColorDefines, Prefab> = new Map<ColorDefines, Prefab>();
    public static initColorPrefabs(complete?: () => void) {
        Const.colorPrefabMap.clear();
        resources.loadDir('./prefabs/bubbles/', (err, data: Prefab[]) => {
            data.forEach(d => {
                let colorDefine = Const.getTypeColorDefines(parseInt(d.name));
                if (colorDefine) {
                    Const.colorPrefabMap.set(colorDefine, d);
                }
                
            });
            complete && complete();
        });
    }

    static getTypeColorDefines(index: number) : ColorDefines {
        switch (index) {
            case 1: return ColorDefines.RED;
            case 2: return ColorDefines.SKY;
            case 3: return ColorDefines.YELLOW;
            case 4: return ColorDefines.GREEN;
            case 5: return ColorDefines.ORANGE;
            case 6: return ColorDefines.PINK;
            case 7: return ColorDefines.TRANSPARENT;
            default: return null;
        }
    }

}

export type NearestFindObject = {position: Vec3, color: ColorDefines};