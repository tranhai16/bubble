import { _decorator, Enum, Node, Prefab} from 'cc';
import { ColorDefines } from './Const';
const { ccclass, property } = _decorator;

@ccclass("ColorPrefab")
export default class ColorPrefab {
    @property({type: Enum(ColorDefines)})
    colorType: ColorDefines;
    @property(Prefab)
    prefab : Prefab;
}