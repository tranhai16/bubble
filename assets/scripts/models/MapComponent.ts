import { _decorator, CircleCollider2D, Component, game, instantiate, Label, Node, Prefab, tween, Tween, UITransform, v3, Vec3 } from 'cc';
import ColorPrefab from './ColorPrefab';
import Level from './Level';
import Rectangle from '../maths/Rectangle';
import { BubbleComponent } from './BubbleComponent';
import Const, { ColorDefines } from './Const';
import GameUtils from '../utils/GameUtils';
const { ccclass, property } = _decorator;

@ccclass('MapComponent')
export class MapComponent extends Component {

    public radiusTile: number = 0;
    public levelColorPrefabs(): IterableIterator<ColorDefines> {
        let set = new Set(this.node.getComponentsInChildren(BubbleComponent).map(n => n.colorType));
        return set.values();
    };
    setLevel(level: Level, complete: () => void) {
        const uiTransform = this.node.getComponent(UITransform);
        const maxCols = level.getMaxCols();
        const radius = uiTransform.contentSize.width / (maxCols * 2);
        this.radiusTile = radius;
        const size = radius * 2;
        let maxWidth = maxCols * size;
        let startX = -maxWidth / 2 + radius;
        let grid = level.grid.reverse();
        let nodeData: { node: Node, position: Vec3 }[] = [];
        for (let i = 0; i < level.rows; i++) {
            const cols = grid[i].length;
            grid[i].forEach((col, index) => {
                let colorPrefab: Prefab = Const.colorPrefabMap.get(col);
                if (colorPrefab) {

                    const position = v3(startX + size * index + (cols === maxCols ? 0 : size * 0.5),
                        i * size * Math.cos(Math.PI / 6), 0);

                    const bubbleNode = instantiate(colorPrefab);
                    this.node.addChild(bubbleNode);
                    bubbleNode.getComponent(BubbleComponent).radius = radius;
                    nodeData.push({ node: bubbleNode, position: position.clone() });
                }

            });
        }
        this.animateStart(nodeData, complete);
    }

    private animateStart(nodeData: { node: Node, position: Vec3 }[], complete: () => void) {
        const positions = nodeData.map(data => data.position);
        let rect = this.getBound(positions);
        let mapHeight = this.node.getComponent(UITransform).contentSize.height;
        let changeY = 0;
        if (rect.getSize().height > mapHeight) {
            if (rect.bottom !== 0) {
                changeY = -rect.bottom;
            }
        }
        else {
            if (rect.top !== mapHeight) {
                changeY = mapHeight - rect.top;
            }
        }
        let count = 0;
        nodeData.reverse().forEach(data => {
            data.position.add3f(0, changeY, 0);
            tween(data.node).to(0.5, { position: data.position }).call(() => {
                count++;
                if (count === nodeData.length) {
                    complete && complete();
                }
            }).start();
        })
    }

    public updateView(complete?: () => void) {
        if (this.isEmpty()) {
            complete && complete()
            return;
        }
        const positions = this.node.children.map(data => data.position);
        let rect = this.getBound(positions);
        let mapHeight = this.node.getComponent(UITransform).contentSize.height;
        let changeY = 0;
        if (rect.getSize().height > mapHeight) {
            if (rect.bottom !== 0) {
                changeY = - rect.bottom;
            }
        }
        else {
            if (rect.top !== mapHeight) {
                changeY = mapHeight - rect.top;
            }
        }
        const holdeDuration = 0.2;
        positions.forEach(data => {
            const childNode = this.node.children.find(child => GameUtils.equalsV3(child.position, data));
            let pos = data.clone().add3f(0, changeY, 0);
            if (childNode) {
                tween(childNode).to(holdeDuration, { position: pos }).start();
            }
        })
        setTimeout(() => {
            complete && complete();
        }, holdeDuration * 1000)
    }

    public getBound(positions?: Vec3[]) : Rectangle {
        !positions && (positions = this.node.children.map(data => data.getPosition()));
        const maxPosition = GameUtils.calculateMaxPosition(positions);
        const minPosition = GameUtils.calculateMinPosition(positions);
        return new Rectangle({
            left: minPosition.x - this.radiusTile,
            right: maxPosition.x + this.radiusTile,
            top: maxPosition.y + this.radiusTile,
            bottom: minPosition.y - this.radiusTile
        });
    }
    public getWorldBound(positions?: Vec3[]) : Rectangle {
        !positions && (positions = this.node.children.map(data => data.getWorldPosition()));
        const maxPosition = GameUtils.calculateMaxPosition(positions);
        const minPosition = GameUtils.calculateMinPosition(positions);
        return new Rectangle({
            left: minPosition.x - this.radiusTile,
            right: maxPosition.x + this.radiusTile,
            top: maxPosition.y + this.radiusTile,
            bottom: minPosition.y - this.radiusTile
        });
    }

    private playTweenPosition(tweens: Tween<Node>[], complete: () => void, count: number = 0) {
        if (count >= tweens.length) {
            complete && complete();
            return;
        }
        tweens[count].call(() => {
            this.playTweenPosition(tweens, complete, count + 1);
        }).start();
    }

    

    public allChildrenWorldPosition(): Vec3[] {
        return this.node.children.map((child) => child.getWorldPosition());
    }

    public removeChildByWorldPosition(worldPosition: Vec3): void {
        this.getChildByWorldPosition(worldPosition)!.removeFromParent();
        // this.addScore(1, worldPosition);
    }


    public getChildByWorldPosition(worldPosition: Vec3): Node {
        return this.node.children.find(child => GameUtils.equalsV3(child.getWorldPosition(), worldPosition));
    }

    public isEmpty(): boolean {
        return this.node.children.length === 0;
    }

    public heighestWorldPositions() : Vec3[] {
        let allWorldPositions = this.allChildrenWorldPosition();
        let maxPos = GameUtils.calculateMaxPosition(allWorldPositions);
        return allWorldPositions.filter(pos => pos.y === maxPos.y);
    }

    public heighestNodes(): Node[] {
        let allMaxPos = this.heighestWorldPositions();
        return this.node.children.filter(child => allMaxPos.find(pos => GameUtils.equalsV3(child.getWorldPosition(), pos)));
    }

    public updateOrphanBubbles(maxPositions: Vec3[], endFallY: number, complete?: (deletePosition?: Vec3[]) => void): void {
        // let maxPositions = this.heighestWorldPositions();
        if (maxPositions.length === 0) {
            this.fallDownOrphanBubbles(this.node.children, endFallY, complete);
            return;
        }
        const list: Vec3[] = [];
        const allPositions = this.allChildrenWorldPosition().filter(v => !maxPositions.find(p => GameUtils.equalsV3(v, p)));
        maxPositions.forEach(position => {
            GameUtils.findNearest(position, this.radiusTile, allPositions).forEach(p => {
                if (!list.find(v3 => GameUtils.equalsV3(p, v3))) {
                    list.push(p);
                }
            })
        });
        const deleteList: Vec3[] = allPositions.filter(position => !list.find(v3 => GameUtils.equalsV3(position, v3)));
        this.fallDownOrphanBubbles(deleteList.map(v => this.getChildByWorldPosition(v)), endFallY, complete);
    }

    public fallDownOrphanBubbles(orphanBubbles: Node[], boundY: number, complete?: (deletePosition?: Vec3[]) => void): void {
        if (orphanBubbles.length === 0) { complete && complete(); return; }
        let count = 0;
        // const positions = orphanBubbles.map(v => v.getWorldPosition());
        const deletePositions: Vec3[] = [];
        orphanBubbles.forEach(n => {
            const wp = n.getWorldPosition();
            tween(n).to(Math.abs(wp.y - boundY) * game.deltaTime / 50, { worldPosition: v3(wp.x, boundY, 0) }, {
                onComplete(target) {
                    deletePositions.push(n.getWorldPosition());
                    n.removeFromParent();
                    count++;
                    if (count >= orphanBubbles.length) {
                        complete && complete(deletePositions);
                    }
                },
            }).start();
        })
    }

}


