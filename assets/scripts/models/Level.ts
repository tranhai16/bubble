import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

export default class Level {
    getMaxCols() : number {
        if (this._grid.length === 0) {return 0;}
        return this._grid.map(x => x.length).sort((a, b) => b - a)[0];
    }
    private _level: number;
    public get level(): number {
        return this._level;
    }
    public set level(value: number) {
        this._level = value;
    }
    private _rows: number;
    public get rows(): number {
        return this._rows;
    }
    public set rows(value: number) {
        this._rows = value;
        this.updateGrid();
    }
    private _grid: number[][] = [];
    public get grid(): number[][] {
        return this._grid;
    }

    constructor(level: number = 0, rows: number = 0) {
        this._level = level;
        this._rows = rows;
        this.updateGrid();
    }

    private updateGrid() {
        this._grid = [];
        for (let i = 0; i < this._rows; i++) {
            this._grid.push([]);
        }
    }

}


